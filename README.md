# README #

Este es el repositorio de la Practica 2 y 3 de l'asignatura M10: Sistemes de gestió empresarial.
Este repositorio ha sido creado por Axel Alavedra.
Version 0.1
Esta abierto al publico para que puedan mejorar el repositorio. Este va a contener datos sobre gatos y perros. Es preciso poner una imagen y documentación de la raza en un fixero particular. Si es posible linkear alguna página relazionada con esta raza.
Con estos datos se creara un programa para buscar cualquier tipo de gato o perro y así obtener información relacionada con este.

Ejemplo:
Raza: Ragdoll
Descripción: Són gatos domésticos, todos tienen pelaje blanco con ojos azules.
[Pagina de ragolls](http://www.els4gats.com/cas/index.html)
![eria1.jpg](https://bitbucket.org/repo/Xqyo64/images/2183093362-eria1.jpg)

Para más info contactar a mi cuenta de BitBucket.